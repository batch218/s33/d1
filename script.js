//API - Application Programming Interface
/*
   - Part of a sserver responsible for receiving request and sending responses
      - Jsut as a Bank Teller stands between a customer and the bank's vault, an API acts as the middleman between in the front and back end
*/

// [SECTION] Javascript Synchronous vs Asyncronous
// Javascript is by default is sychrounous meaning that only one statement is executed at a time.

// This can be proven when a statement has an error, javascript will not proceed with the next statement

// console.log("Hello World");
// console.lo("Hello again"); // Syntax error
// console.log("Goodbye");

// for(let i=0; i<=1500; i++){
//   console.log(i);
// }

// console.log("Hello");

//Asyncronous
// - means that we can proceed to execute other statements, while time consuming code/s is running in the background


// The fetch API allows you to asyncronousle request for a resource data
// A "promise" is an object that represent the eventual completion (or failure) of Synchronous function and its resulting value
// Syntax: fectch('URL');
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts/')

// The "then" method capture the "Response" object and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts/')

//Use the "json" method from the "reponse" onject and returns another "promise"
.then((response)=> response.json())

//Print converted JSON value from the "fetch requet"
.then((json)=> console.log(json))


// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code

async function fetchData(){
     let result = await fetch("https://jsonplaceholder.typicode.com/posts")
    console.log(result);

    let json = await result.json();
    console.log(json);
}
let speciificDocument = fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json()) 
.then((json)=>console.log(json))
//[SECTION] Getting a specific document

//retrieving titles of the whole collection
// fetch('https://jsonplaceholder.typicode.com/posts') // fetch the whole collection
// .then((response) => response.json()) // it converts out json object reponse to js object

// .then((json) => {json.forEach(post => console.log(post.title))});
fetch('https://jsonplaceholder.typicode.com/posts/87')
.then((response) => response.json())
.then((json) => console.log(`The retrieved title ${json.title}`));


//[SECTION] Creating a specific post
fetch("https://jsonplaceholder.typicode.com/posts",{
  method: 'POST',
  headers:{
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    title: 'OOTD',  
    body: 'New Outfit',
    userID: 1
  })
})
.then((response)=>response.json())
.then((json)=> console.log(json));


// [SECTION] Updating a post PUT method 

// fetch("https://jsonplaceholder.typicode.com/posts/1",{
//   method: 'PUT',
//   headers:{
//     'Content-type': 'application/json'
//   },
//   body: JSON.stringify({
//     id: 1,
//     title: "New Car",
//     body: "Just got my new red tesla ",
//     userID: 1
//   })
// })
// .then((response)=>response.json())
// .then((json)=> console.log(json));


// [SECTION] Updating a post using PATCH method
  //Patch vs PUT
  // PATCH is used to update a single/several properties
  //PUT is used to update all properties
fetch("https://jsonplaceholder.typicode.com/posts/1",{
  method: 'PATCH',
  headers:{
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    title: "Corrected post title",
  })
})
.then((response)=>response.json())
.then((json)=> console.log(json));



fetch("https://jsonplaceholder.typicode.com/posts/1",{
  method: 'DELETE',
  
})
.then((response)=>response.json())
.then((json)=> console.log("Successfull Deletion",json));
